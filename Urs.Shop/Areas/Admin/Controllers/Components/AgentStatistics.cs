﻿using Microsoft.AspNetCore.Mvc;
using Urs.Core;
using Urs.Services.Agents;
using Urs.Services.Security;

namespace Urs.Admin.Components
{
    public class AgentStatisticsViewComponent : ViewComponent
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IAgentService _agentService;

        #endregion

        #region Ctor

        public AgentStatisticsViewComponent(
            IPermissionService permissionService,
            IWorkContext workContext,
            IAgentService agentService)
        {
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._agentService = agentService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Invoke view component
        /// </summary>
        /// <returns>View component result</returns>
        public IViewComponentResult Invoke()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAgents))
                return Content("");

            var summary = _agentService.GetAgentSummary();
            return View(summary);
        }

        #endregion
    }
}