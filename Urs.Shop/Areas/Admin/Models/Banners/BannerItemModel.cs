﻿using System.ComponentModel.DataAnnotations;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Banners
{
    public partial class BannerItemModel : BaseEntityModel
    {
        public int BannerZoneId { get; set; }
        public virtual string Title { get; set; }
        public virtual string SubTitle { get; set; }
        public virtual string Url { get; set; }

        [UIHint("Picture")]
        public virtual int PictureId { get; set; }
        public virtual string PictureUrl { get; set; }

        public virtual int DisplayOrder { get; set; }
    }
}