﻿using Microsoft.AspNetCore.Mvc;
using Urs.Framework;
using Urs.Framework.Mvc;

using Urs.Framework.Kendoui;

namespace Urs.Admin.Models.Stores
{
    public partial class CategoryListModel : BaseModel
    {
        [UrsDisplayName("Admin.Store.Categories.List.SearchCategoryName")]
        
        public string SearchCategoryName { get; set; }

        public ResponseResult Categories { get; set; }
    }
}