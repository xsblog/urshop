﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Urs.Framework;
using Urs.Framework.Mvc;

using Urs.Framework.Kendoui;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Urs.Admin.Models.Users
{
    public partial class UserListModel : BaseModel
    {
        public UserListModel()
        {
            SearchUserRoleIds = new List<int>(); 
            AvailableUserRoles = new List<SelectListItem>();
        }
        [UrsDisplayName("Admin.Users.Users.List.ProvinceId")]
        public string ProvinceName { get; set; }
        [UrsDisplayName("Admin.Users.Users.List.CityId")]
        public string CityName { get; set; }
        [UrsDisplayName("Admin.Users.Users.List.AreaId")]
        public string AreaName { get; set; }
        public ResponseResult Users { get; set; }

        [UrsDisplayName("Admin.Users.Users.List.UserRoles")]
        
        public List<SelectListItem> AvailableUserRoles { get; set; }
        [UrsDisplayName("Admin.Users.Users.List.UserRoles")]
        [UIHint("MultiSelect")]
        public IList<int> SearchUserRoleIds { get; set; }

        [UrsDisplayName("Admin.Users.Users.List.SearchEmail")]
        
        public string SearchEmail { get; set; }

        [UrsDisplayName("Admin.Users.Users.List.SearchUsername")]
        public string SearchNickname { get; set; }

        [UrsDisplayName("Admin.Users.Users.List.SearchCompany")]
        public string SearchCompany { get; set; }
        public bool CompanyEnabled { get; set; }

        [UrsDisplayName("Admin.Users.Users.List.SearchPhone")]
        
        public string SearchPhone { get; set; }
        public bool PhoneEnabled { get; set; }

    }
}