﻿using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public partial class UserReportsModel : BaseModel
    {
        public BestUsersReportModel BestUsersByOrderTotal { get; set; }
        public BestUsersReportModel BestUsersByNumberOfOrders { get; set; }
    }
}