﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Users
{
    public class MessageRecodeItemModel:BaseEntityModel
    {
        /// <summary>
        /// 短信内容
        /// </summary>
        [UrsDisplayName("Admin.Users.Users.Fields.Content")]
        public virtual string Content { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.UserId")]
        /// <summary>
        /// 发送人Id
        /// </summary>
        public virtual int UserId { get; set; }
        public virtual string UserName { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Phone")]
        /// <summary>
        /// 接收号码
        /// </summary>
        public virtual string Phone { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.SendTime")]
        /// <summary>
        /// 发送时间
        /// </summary>
        public virtual DateTime SendTime { get; set; }
        [UrsDisplayName("Admin.Users.Users.Fields.Mark")]
        /// <summary>
        /// 标志
        /// </summary>
        public virtual string Mark { get; set; }
    }
}