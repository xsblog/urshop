﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Urs.Framework;
using Urs.Framework.Models;
using Urs.Framework.Mvc;

namespace Urs.Admin.Models.Settings
{
    public class TemplateSettingsModel : BaseModel, ISettingsModel
    {
        public TemplateSettingsModel()
        {
            DefaultArray = new List<SelectListItem>();
            DefaultArray.Add(new SelectListItem() { Value = "1", Text = "模板1" });
            DefaultArray.Add(new SelectListItem() { Value = "2", Text = "模板2" });
            DefaultArray.Add(new SelectListItem() { Value = "3", Text = "模板3" });
        }
        [UrsDisplayName("Admin.Configuration.Template.Picture")]
        [UIHint("Picture")]
        public int Picture1Id { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Text")]
        public string Text1 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.SubText")]
        public string SubText1 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Time")]
        public string Time1 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Ext")]
        public string Ext1 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.AltText")]
        public string AltText1 { get; set; }

        [UrsDisplayName("Admin.Configuration.Template.Picture")]
        [UIHint("Picture")]
        public int Picture2Id { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Text")]
        public string Text2 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.SubText")]
        public string SubText2 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Time")]
        public string Time2 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Ext")]
        public string Ext2 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.AltText")]
        public string AltText2 { get; set; }


        [UrsDisplayName("Admin.Configuration.Template.Picture")]
        [UIHint("Picture")]
        public int Picture3Id { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Text")]
        public string Text3 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.SubText")]
        public string SubText3 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Time")]
        public string Time3 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.Ext")]
        public string Ext3 { get; set; }
        [UrsDisplayName("Admin.Configuration.Template.AltText")]
        public string AltText3 { get; set; }

        [DisplayName("默认")]
        public int Default { get; set; }


        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId1 { get; set; }
        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId2 { get; set; }
        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId3 { get; set; }
        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId4 { get; set; }
        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId5 { get; set; }
        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId6 { get; set; }
        [UrsDisplayName("图片")]
        [UIHint("Picture")]
        public int TianQiId7 { get; set; }

        [DisplayName("启动")]
        public bool Enabled { get; set; }
        public List<SelectListItem> DefaultArray { get; set; }
    }
}