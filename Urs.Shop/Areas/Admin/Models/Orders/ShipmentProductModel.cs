﻿using Urs.Framework;
using Urs.Framework.Models;

namespace Urs.Admin.Models.Orders
{
    public partial class ShipmentGoodsModel : BaseEntityModel
    {
        [UrsDisplayName("Admin.Orders.Shipments.OrderID")]
        public int OrderId { get; set; }
        public string ShippingStatus { get; set; }
        public int GoodsId { get; set; }
        public string GoodsName { get; set; }
        public string AttributeInfo { get; set; }
        public string Sku { get; set; }
        public string ItemWeight { get; set; }
        public string ItemDimensions { get; set; }
        public int Quantity { get; set; }
        public string TotalWeight { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.ShippingMethod")]
        public string ShippingMethod { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.ExpressName")]
        public string ExpressName { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.TrackingNumber")]
        public string TrackingNumber { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.ShippedDate")]
        public string ShippedDate { get; set; }
        public bool CanShip { get; set; }
        [UrsDisplayName("Admin.Orders.Shipments.DeliveryDate")]
        public string DeliveryDate { get; set; }
        public bool CanDeliver { get; set; }

    }
}