﻿using Microsoft.AspNetCore.Mvc;

namespace Urs.Web.Controllers
{
    public partial class KeepAliveController : Controller
    {
        public virtual IActionResult Index()
        {
            return Content("I am UrShop!");
        }
    }
}