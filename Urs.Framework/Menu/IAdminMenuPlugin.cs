﻿using Urs.Core.Plugins;

namespace Urs.Framework.Menu
{
    public interface IAdminMenuPlugin : IPlugin
    {
        void ManageSiteMap(SiteMapNode rootNode);
    }
}
