
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Stores;

namespace Urs.Data.Mapping.Stores
{
    public partial class GoodsSpecMap : UrsEntityTypeConfiguration<GoodsSpec>
    {
        public override void Configure(EntityTypeBuilder<GoodsSpec> builder)
        {
            builder.ToTable(nameof(GoodsSpec));
            builder.HasKey(pa => pa.Id);
            builder.Property(pa => pa.Name).IsRequired();
            base.Configure(builder);
        }
    }
}