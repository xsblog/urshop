
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Urs.Data.Domain.Orders;
using Urs.Data.Domain.Users;

namespace Urs.Data.Mapping.Users
{
    public class PointsHistoryMap : UrsEntityTypeConfiguration<PointsHistory>
    {
        public override void Configure(EntityTypeBuilder<PointsHistory> builder)
        {
            builder.ToTable(nameof(PointsHistory));
            builder.HasKey(rph => rph.Id);

            builder.Property(rph => rph.UsedAmount).HasColumnType("decimal(18, 4)");

            builder.HasOne(rph => rph.User)
                .WithMany(c => c.PointsHistory)
                .HasForeignKey(rph => rph.UserId);

            builder.HasOne(rph => rph.UsedWithOrder)
                .WithOne(order=>order.PointsEntry)
                .HasForeignKey<Order>(order=>order.RewardPointsHistoryEntryId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            base.Configure(builder);
        }
    }
}