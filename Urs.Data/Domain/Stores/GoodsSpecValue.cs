using Urs.Core;
namespace Urs.Data.Domain.Stores
{
    /// <summary>
    /// ���ֵ
    /// </summary>
    public partial class GoodsSpecValue : BaseEntity
    {
        /// <summary>
        /// Gets or sets the goods attribute mapping identifier
        /// </summary>
        public virtual int GoodsSpecId { get; set; }
        /// <summary>
        /// Gets or sets the goods attribute name
        /// </summary>
        public virtual string Name { get; set; }

        public bool Deleted { get; set; }
    }

}
