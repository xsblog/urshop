﻿using Urs.Core.Configuration;

namespace Urs.Data.Domain.Configuration
{
    public class StoreInformationSettings : ISettings
    {
        /// <summary>
        /// 网站名称
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 网站网址
        /// </summary>
        public string StoreUrl { get; set; }

        public int StoreLogo1 { get; set; }
        public int StoreLogo2 { get; set; }
        public int StoreLogo3 { get; set; }
        /// <summary>
        /// 版权信息
        /// </summary>
        public string CopyrightInfo { get; set; }
        /// <summary>
        /// 备案号
        /// </summary>
        public string ICPRecord { get; set; }
        /// <summary>
        /// 网站服务时间
        /// </summary>
        public string StoreServiceTime { get; set; }
        /// <summary>
        /// 网站服务电话
        /// </summary>
        public string ServiceTelePhone { get; set; }
        /// <summary>
        /// 在线客服代码
        /// </summary>
        public string OnlineConsultingCode { get; set; }
        public string IconTip { get; set; }
        public string ServiceTip { get; set; }
        /// <summary>
        /// 网站关闭
        /// </summary>
        public bool StoreClosed { get; set; }
        public string StoreKey { get; set; }
        public string StoreSecret { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether administrators can visit a closed store
        /// </summary>
        public bool StoreClosedAllowForAdmins { get; set; }

    }
}
