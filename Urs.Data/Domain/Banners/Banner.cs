using Urs.Core;
using System.Collections.Generic;

namespace Urs.Data.Domain.Banners
{
    public partial class Banner : BaseEntity
    {
     

        private ICollection<BannerItem> _items;

        public virtual string Name { get; set; }
        public virtual string ClassName { get; set; }

        public virtual string ZoneName { get; set; }
        public virtual string TemplateName { get; set; }
        public virtual ICollection<BannerItem> Items
        {
            get { return _items ?? (_items = new List<BannerItem>()); }
            protected set { _items = value; }
        }

    }
}