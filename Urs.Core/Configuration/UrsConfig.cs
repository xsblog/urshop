namespace Urs.Core.Configuration
{
    /// <summary>
    /// Represents a UrsConfig
    /// </summary>
    public partial class UrsConfig 
    {
        public bool ClearPluginShadowDirectoryOnStartup { get; set; }
        public bool CopyLockedPluginAssembilesToSubdirectoriesOnStartup { get; set; }
        public bool UseUnsafeLoadAssembly { get; set; }
        public bool UsePluginsShadowCopy { get; set; }
        public bool UseSessionStateTempDataProvider { get; set; }
    }
}
