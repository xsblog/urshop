﻿
namespace Urs.Core
{
    public static class UrsVersion
    {
        /// <summary>
        /// Gets or sets the store version
        /// </summary>
        public static string CurrentVersion { get; } = "2.00";
    }
}
