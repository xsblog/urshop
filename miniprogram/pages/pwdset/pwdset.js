// pages/pwdset/pwdset.js
import api from '../../api/api'
import { sendsmsforfindpwd } from '../../api/conf'
import { pwdset } from '../../api/conf'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    yzmtxt: '发送验证码',
    yzmTime: 60,
    phone: '',
    newPwd: '',
    yzm: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  sendyzm: function () {
    if (this.data.phone == '') {
      wx.showToast({
        title: '手机号码不能为空',
        icon: 'none',
        duration: 2000
      })
      return
    }
    if (this.data.yzmTime != 60) {
      return
    }
    var that = this
    api.post(sendsmsforfindpwd + '?phone=' + that.data.phone, {
    }).then((res) => {
      var timer = setInterval(() => {
        if (that.data.yzmTime == 1) {
          clearInterval(timer)
          that.setData({
            yzmtxt: '发送验证码',
            yzmTime: 60
          })
          return
        }
        that.data.yzmTime--;
        that.setData({
          yzmtxt: '倒计时' + that.data.yzmTime + '秒'
        })
      }, 1000)
    }).catch((err) => {
      wx.showToast({
        title: err.Content,
        icon: 'none',
      })
    })
  },
  getphone: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  getNewPwd: function(e){
    this.setData({
      newPwd: e.detail.value
    })
  },
  getyzm: function(e){
    this.setData({
      yzm: e.detail.value
    })
  },
  pwdset: function(){
    var that = this
    wx.showLoading({
      title: '加载中',
    })
    api.post(pwdset,{
      Phone: that.data.phone,
      Token: that.data.yzm,
      NewPassword: that.data.newPwd
    }).then((res)=>{
      wx.hideLoading()
      wx.showToast({
        title: '修改成功',
      })
      wx.navigateTo({
        url: '/pages/login/login',
      })
    }).catch((err)=>{
      wx.showToast({
        title: err.Content,
        icon: 'none'
      })
    })
  }
})