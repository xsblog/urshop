﻿using System.Collections.Generic;
using Urs.Data.Domain.Common;

namespace Plugin.Api.Models.User
{
    /// <summary>
    /// 等级信息
    /// </summary>
    public partial class MoAllLevel
    {
        /// <summary>
        /// 等级Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 等级编号
        /// </summary>
        public int Grade { get; set; }
        /// <summary>
        /// 等级名称
        /// </summary>
        public  string Name { get; set; }
        /// <summary>
        /// 是否有最低积分限制
        /// </summary>
        public  bool PointsLowerLimit { get; set; }
        /// <summary>
        /// 积分下限
        /// </summary>
        public  decimal PointsLowerLimitAmount { get; set; }
        /// <summary>
        /// 积分上限
        /// </summary>
        public  decimal PointsUpperLimitAmount { get; set; }
        /// <summary>
        /// 购买赠送积分
        /// </summary>
        public  int PointsForPurchases_Points { get; set; }
    }
}