﻿using System.Collections.Generic;
using Urs.Data.Domain.Stores;
using Urs.Data.Domain.Common;
using Plugin.Api.Models.Catalog;
using Plugin.Api.Models.Media;

namespace Plugin.Api.Models.Goods
{
    /// <summary>
    /// 商品详情
    /// </summary>
    public partial class MoGoods
    {
        public MoGoods()
        {
            Pictures = new List<MoPicture>();
            Specs = new List<MoSpecification>();
            Attrs = new List<MoGoodsAttr>();
            AttrValueList = new List<MoGoodsAttrValueList>();
            Fields = new List<MoCustomField>();
            Tags = new List<MoGoodsTag>();
        }
        /// <summary>
        /// 商品Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// sku
        /// </summary>
        public string Sku { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string Short { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Full { get; set; }
        /// <summary>
        /// 库存数量
        /// </summary>
        public int StockQuantity { get; set; }
        /// <summary>
        /// 市场价
        /// </summary>
        public string OldPrice { get; set; }
        /// <summary>
        /// 商城价
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// 成本价
        /// </summary>
        public string ProductCost { get; set; }
        /// <summary>
        /// 是否发布上架
        /// </summary>
        public bool Published { get; set; }
        /// <summary>
        /// 商品销量
        /// </summary>
        public int SaleVolume { get; set; }
        /// <summary>
        /// 商品评分
        /// </summary>
        public int RatingSum { get; set; }
        /// <summary>
        /// 总评论
        /// </summary>
        public int TotalReviews { get; set; }
        /// <summary>
        /// 可以用空格 逗号进行分组展示多个icon行
        /// </summary>
        public string IconTip { get; set; }
        /// <summary>
        /// 商品服务说明
        /// </summary>
        public string ServiceTip { get; set; }
        /// <summary>
        /// 商品图片
        /// </summary>
        public IList<MoPicture> Pictures { get; set; }
        /// <summary>
        /// 规格参数
        /// </summary>
        public IList<MoSpecification> Specs { get; set; }
        /// <summary>
        /// 商品标签
        /// </summary>
        public IList<MoGoodsTag> Tags { get; set; }
        /// <summary>
        /// 商品自定义属性
        /// </summary>
        public IList<MoGoodsAttr> Attrs { get; set; }

        public IList<MoGoodsAttrValueList> AttrValueList { get; set; }
        /// <summary>
        /// 自定义字段
        /// </summary>
        public IList<MoCustomField> Fields { get; set; }

        public partial class MoCustomField
        {
            /// <summary>
            /// Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 默认值
            /// </summary>
            public string DefaultValue { get; set; }
        }

        #region Nested Classes
        public partial class MoGoodsTag
        {
            /// <summary>
            /// 标签编号
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 标签名称
            /// </summary>
            public string Name { get; set; }
        }
        /// <summary>
        /// 商品自定义属性
        /// </summary>
        public partial class MoGoodsAttr
        {
            public MoGoodsAttr()
            {
                Values = new List<MoGoodsAttrValue>();
            }
            /// <summary>
            /// 属性Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 商品Id
            /// </summary>
            public int GoodsId { get; set; }
            /// <summary>
            /// 商品属性Id
            /// </summary>
            public int GoodsSpecId { get; set; }
            /// <summary>
            /// 属性名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 属性提示语
            /// </summary>
            public string TextPrompt { get; set; }
            /// <summary>
            /// 是否必须
            /// </summary>
            public bool IsRequired { get; set; }
            /// <summary>
            /// 属性值
            /// </summary>
            public IList<MoGoodsAttrValue> Values { get; set; }
        }
        /// <summary>
        /// 商品属性值
        /// </summary>
        public partial class MoGoodsAttrValue
        {
            /// <summary>
            /// 属性值Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 属性名称
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 属性价格调整
            /// </summary>
            public string PriceAdjustment { get; set; }
            /// <summary>
            /// 属性价格值
            /// </summary>
            public decimal PriceAdjustmentValue { get; set; }
            /// <summary>
            /// 是否预选
            /// </summary>
            public bool IsPreSelected { get; set; }
        }
        /// <summary>
        /// 商品组合属性
        /// </summary>
        public partial class MoGoodsAttrValueList
        {
            /// <summary>
            /// 组合属性Id
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 商品Id
            /// </summary>
            public int GoodsId { get; set; }
            /// <summary>
            /// 规格Id串
            /// </summary>
            public string AttributeValue { get; set; }
            /// <summary>
            /// 规格内容Xml
            /// </summary>
            public string AttributesXml { get; set; }
            /// <summary>
            /// 商品数量
            /// </summary>
            public int Qty { get; set; }
            /// <summary>
            /// Sku
            /// </summary>
            public string Sku { get; set; }
            /// <summary>
            /// 价格
            /// </summary>
            public string Price { get; set; }
        }
        #endregion


    }
}