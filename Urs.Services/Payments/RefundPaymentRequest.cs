﻿using Urs.Data.Domain.Orders;

namespace Urs.Services.Payments
{
    public partial class RefundPaymentRequest
    {
        public Order Order { get; set; }

        public decimal AmountToRefund { get; set; }

        public bool IsPartialRefund { get; set; }
    }
}
