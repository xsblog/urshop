using System.Collections.Generic;
using Urs.Core;
using Urs.Data.Domain.Stores;

namespace Urs.Services.Stores
{
    public partial interface IBrandService
    {
        void Delete(Brand brands);

        IList<Brand> GetAll(bool showHidden = false);

        IList<Brand> GetAll(string brandName, bool showHidden = false);

        IList<Brand> ShowOnPublicPage(bool showHidden = false);

        IPagedList<Brand> GetAll(string brandsName,
            int pageIndex, int pageSize, bool showHidden = false);

        Brand GetById(int brandsId);

        void Insert(Brand brands);

        void Update(Brand brands);

        void DeleteGoodsBrand(GoodsBrandMapping goodsBrand);

        IPagedList<GoodsBrandMapping> GetGoodsBrandsByBrandId(int brandsId,
            int pageIndex, int pageSize, bool showHidden = false);
        
        int GetTotalNumberOfFeaturedGoodss(int brandsId);

        IList<Goods> GetFeaturedGoodss(int brandsId, int count);
        GoodsBrandMapping GetGoodsBrandById(int goodsBrandId);

        void InsertGoodsBrand(GoodsBrandMapping goodsBrand);

        void UpdateGoodsBrand(GoodsBrandMapping goodsBrand);
    }
}
