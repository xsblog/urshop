using System.Collections.Generic;
using Urs.Data.Domain.Users;
using Urs.Data.Domain.Security;

namespace Urs.Services.Security
{
    public partial interface IPermissionService
    {
        void DeletePermissionRecord(PermissionRecord permission);

        PermissionRecord GetPermissionRecordById(int permissionId);

        PermissionRecord GetPermissionRecordBySystemName(string systemName);

        IList<PermissionRecord> GetAllPermissionRecords(string group = "");
        IList<string> GetAllGroup();
        void InsertPermissionRecord(PermissionRecord permission);

        void UpdatePermissionRecord(PermissionRecord permission);

        bool Authorize(PermissionRecord permission);

        bool Authorize(PermissionRecord permission, User user);

        bool Authorize(string permissionRecordSystemName);

        bool Authorize(string permissionRecordSystemName, User user);

        IList<PermissionRecordUserMapping> GetPermissionRecordUserMappings();

        IList<PermissionRecordUserMapping> GetPermissionRecordUserMappings(int userRoleId);
    }
}