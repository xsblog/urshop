using System.Collections.Generic;
using Urs.Data.Domain.Topics;
using Urs.Core;

namespace Urs.Services.Topics
{
    public partial interface ITopicService
    {
        #region Topic

        void DeleteTopic(Topic topic);

        Topic GetTopicById(int topicId);

        Topic GetTopicBySystemName(string systemName);

        IList<Topic> GetAllTopics();

        IPagedList<Topic> GetAllTopics(string name, int pageIndex, int pageSize);

        void InsertTopic(Topic topic);

        void UpdateTopic(Topic topic);
        #endregion

        #region Topic Category

        void DeleteTopicCategory(TopicCategory category);
        TopicCategory GetTopicCategoryById(int categoryId);
        IPagedList<TopicCategory> GetAllTopicCategory(int pageIndex, int pageSize, bool showHidden = false);
        IList<TopicCategory> GetAllTopicCategory(bool showHidden = false);
        void InsertTopicCategory(TopicCategory category);
        void UpdateTopicCategory(TopicCategory category);

        #endregion
    }
}
