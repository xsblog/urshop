namespace Urs.Services.Media
{
    public enum ResizeType
    {
        LongestSide,

        Width,

        Height
    }
}
